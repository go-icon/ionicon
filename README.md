# ionicon
https://ionicons.com/

# Installation
`go get -u gitea.com/go-icon/ionicon`

# Usage
```go
icon := ionicon.IosAdd()

// Get the raw XML
xml := icon.XML()

// Get something suitable to pass directly to an html/template
html := icon.HTML()
```

# Build
`go generate generate.go`

# New Versions
To update the version of ionicon, simply change `ioniconVersion` in `ionicon_generate.go` and re-build.

# License
[MIT License](LICENSE)